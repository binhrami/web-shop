set :application, 'webshop.tranlocai.com'
set :branch, 'master'
set :format, :pretty
append :linked_dirs, "node_modules"

# api server
server '120.72.98.141', user: 'agilelab', roles: %w{app}

set :ssh_options, {
  keys: %w(~/.ssh/id_rsa),
  forward_agent: false,
  auth_methods: %w(publickey)
#  verbose: :info
}

namespace :deploy do
  task :build_web do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{current_path} && yarn build:pro"
    end
  end
  task :build_admin do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{current_path} && yarn build:admin:pro"
    end
  end
  desc 'Restart web'
  task :restart_web do
    on roles(:app), in: :sequence, wait: 5 do
      execute :sudo, "systemctl restart #{fetch(:application)}"
    end
  end
  desc 'Restart Admin'
  task :restart_admin do
    on roles(:app), in: :sequence, wait: 5 do
      execute :sudo, "systemctl restart webshopadmin.tranlocai.com"
    end
  end
  desc 'Restart nginx'
  task :restart_nginx do
    on roles(:app), in: :sequence, wait: 5 do
      execute :sudo, "systemctl restart nginx"
    end
  end
  after :publishing, :build_web
  after :publishing, :build_admin
  after :publishing, :restart_web
  after :publishing, :restart_admin
  after :publishing, :restart_nginx
end
