
set :repo_url, "git@gitlab.com:binhrami/web-shop.git"
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/secrets.yml config/database.yml}
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
# set :linked_dirs, %w{log}

set :keep_releases, 10
set :rbenv_type, :system
set :rbenv_ruby, "2.6.3" # change to 2.6.3 to deploy prelive
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all

# SSHKit.conifg
SSHKit.config.command_map[:rake] = 'bundle exec rake'
