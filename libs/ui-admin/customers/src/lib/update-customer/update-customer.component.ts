import { Component, OnInit } from "@angular/core";
import { UpdateCustomerForm } from "./update-customer.form";
import { ActivatedRoute, Router } from "@angular/router";
import { CustomerFacade } from "../ngrx/customer.facade";
import { Customer } from "../ngrx/customer.model";
import { Observable } from "rxjs";
import { FormGroup } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { filter, take } from "rxjs/operators";

@Component({
  selector: "seed-update-customer",
  templateUrl: "./update-customer.component.html",
  styleUrls: ["./update-customer.component.scss"]
})
export class UpdateCustomerComponent implements OnInit {
  customer$: Observable<Customer> = this.customerFacade.selectedCustomer$;
  idCustomer: string;
  formUpdateCustomer: FormGroup ;

  constructor(private route: ActivatedRoute, private customerFacade: CustomerFacade, private router: Router, private toastService: ToastrService) {
    this.customerFacade.selectedCustomer$.pipe(take(2), filter(Boolean)).subscribe(customer =>
      this.formUpdateCustomer = UpdateCustomerForm.create(customer)
    );
    this.idCustomer = this.route.snapshot.params["id"];
    this.customerFacade.getCustomerById(this.idCustomer);


  }

  ngOnInit() {
  }

  navigateCustomer() {
    return this.router.navigate(["/customers"]);
  }

  updateCustomer() {
    const { email, fullName, phone, subject, address, messages } = this.formUpdateCustomer.value;
    const body: Customer = {
      email: email,
      fullName: fullName,
      phone: phone,
      subject: subject,
      address: address,
      messages: messages
    };
    this.customerFacade.updateCustomer(this.idCustomer, body);
    this.toastService.success("Cập nhật khách hàng thành công !");
    return this.router.navigate(["/customers"]);


  }

}
