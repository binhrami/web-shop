import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Customer } from "../ngrx/customer.model";

export interface UpdateCustomerForm {
  fullName: string
  email: string
  phone: string
  address: string
  subject: string
  messages: string
}

export const UpdateCustomerForm = {
  create: (customer: Customer) => {
    const form = new FormGroup({
      fullName: new FormControl(customer.fullName, [
        Validators.required
      ]),
      email: new FormControl(customer.email, [Validators.required, Validators.email]),
      phone: new FormControl(customer.phone, [Validators.required]),
      address: new FormControl(customer.address),
      subject: new FormControl(customer.subject, [Validators.required]),
      messages: new FormControl(customer.messages)
    });


    return form;
  }
};
