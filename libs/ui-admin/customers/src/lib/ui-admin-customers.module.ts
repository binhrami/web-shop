import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CustomerComponent } from "./customer/customer.component";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import {
  CUSTOMER_FEATURE_KEY,
  initialState as customerInitialState,
  customerReducer
} from "./ngrx/customer.reducer";
import { CustomerEffects } from "./ngrx/customer.effects";
import { CustomerFacade } from "./ngrx/customer.facade";
import { RouterModule } from "@angular/router";
import {
  MatButtonModule,
  MatCardModule, MatDialogModule, MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatTableModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { TranslateModule } from "@ngx-translate/core";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { UpdateCustomerComponent } from "./update-customer/update-customer.component";
import { DialogCreateCustomerComponent } from "./dialog/dialog-create-customer.component";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    TranslateModule,
    FontAwesomeModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false }),
    RouterModule.forChild([
      {
        path: "",
        component: CustomerComponent
      },
      {
        path: ":id",
        component: UpdateCustomerComponent
      }
    ]),
    StoreModule.forFeature(CUSTOMER_FEATURE_KEY, customerReducer, {
      initialState: customerInitialState
    }),
    EffectsModule.forFeature([CustomerEffects])
  ],
  declarations: [CustomerComponent, UpdateCustomerComponent, DialogCreateCustomerComponent],
  entryComponents: [DialogCreateCustomerComponent],
  providers: [CustomerFacade]
})
export class UiAdminCustomersModule {
}
