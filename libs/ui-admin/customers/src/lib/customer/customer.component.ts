import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { CustomerFacade } from "../ngrx/customer.facade";
import { DialogCreateCustomerComponent } from "../dialog/dialog-create-customer.component";
import { MatDialog } from "@angular/material";
import { Customer } from "../ngrx/customer.model";
import { Observable } from "rxjs";
import { ToastrService } from "ngx-toastr";


@Component({
  selector: "seed-customer",
  templateUrl: "./customer.component.html",
  styleUrls: ["./customer.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class CustomerComponent implements OnInit {


  displayedColumns = ["fullName", "email", "phone", "address", "subject", "message"];
  dataSource$: Observable<Customer[]> = this.customerFacade.allCustomer$;

  constructor(private customerFacade: CustomerFacade, private dialog: MatDialog,
              private toastService: ToastrService
  ) {
    this.customerFacade.loadAll();

  }

  ngOnInit() {
  }

  addNew() {
    const dialogRef = this.dialog.open(DialogCreateCustomerComponent, {
      disableClose: true,
      width: "800px"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.toastService.success(
           'Lưu khách hàng thành công !',
        );
        this.customerFacade.loadAll();
      }
    });
  }

}



