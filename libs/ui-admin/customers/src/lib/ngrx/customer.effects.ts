import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";
import { DataPersistence } from "@nrwl/nx";

import { CustomerPartialState } from "./customer.reducer";
import {
  LoadCustomer,
  CustomerLoaded,
  CustomerLoadError,
  CustomerActionTypes,
  SaveCustomer,
  SaveCustomerSuccessfully,
  SaveCustomerError,
  UpdateCustomer,
  UpdateCustomerSuccess,
  UpdateCustomerError
} from "./customer.actions";
import { BaseService } from "@seed/common/core";
import { map } from "rxjs/operators";
import { Customer } from "./customer.model";


@Injectable()
export class CustomerEffects {
  @Effect() loadCustomer$ = this.dataPersistence.fetch(
    CustomerActionTypes.LoadCustomer,
    {
      run: (action: LoadCustomer, state: CustomerPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return this.baseService.get("/customer").pipe(map((res: Customer[]) => new CustomerLoaded(res)))
      },

      onError: (action: LoadCustomer, error) => {
        console.error("Error", error);
        return new CustomerLoadError(error);
      }
    }
  );

  @Effect() saveCustomer$ = this.dataPersistence.fetch(
    CustomerActionTypes.SaveCustomer,
    {
      run: (action: SaveCustomer, state: CustomerPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        return this.baseService.post("/customer/create", action.payload).pipe(map((res: Customer) => new SaveCustomerSuccessfully(res)))
      },

      onError: (action: SaveCustomer, error) => {
        console.error("Error", error);
        return new SaveCustomerError(error);
      }
    }
  );

  @Effect() updateCustomer$ = this.dataPersistence.fetch(
    CustomerActionTypes.UpdateCustomer,
    {
      run: (action: UpdateCustomer, state: CustomerPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        // return new ServiceManagementLoaded([]);

        return this.baseService.put(`/customer/${action.customerId}`, action.body).pipe(map((res: any) =>  new UpdateCustomerSuccess(res) ))
      },

      onError: (action: UpdateCustomer, error) => {
        console.error("Error", error);
        return new UpdateCustomerError(error);
      }
    }
  );

  constructor(
    private baseService: BaseService,
    private actions$: Actions,
    private dataPersistence: DataPersistence<CustomerPartialState>
  ) {}
}
