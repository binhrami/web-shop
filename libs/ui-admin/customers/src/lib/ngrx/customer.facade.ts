import { Injectable } from "@angular/core";

import { select, Store } from "@ngrx/store";

import { CustomerPartialState } from "./customer.reducer";
import { customerQuery } from "./customer.selectors";
import { GetCustomerById, LoadCustomer, SaveCustomer, UpdateCustomer } from "./customer.actions";
import { Customer } from "./customer.model";

@Injectable()
export class CustomerFacade {
  loaded$ = this.store.pipe(select(customerQuery.getLoaded));
  allCustomer$ = this.store.pipe(select(customerQuery.getAllCustomer));
  selectedCustomer$ = this.store.pipe(
    select(customerQuery.getSelectedCustomer)
  );

  constructor(private store: Store<CustomerPartialState>) {}

  loadAll() {
    this.store.dispatch(new LoadCustomer());
  }

  createCustomer(body: Customer) {
    this.store.dispatch(new SaveCustomer(body));
  }

  getCustomerById(customerId: string) {
    this.store.dispatch(new GetCustomerById(customerId));
  }

  updateCustomer(customerId: string, body: Customer) {
    this.store.dispatch(new UpdateCustomer(customerId, body))
  }
}
