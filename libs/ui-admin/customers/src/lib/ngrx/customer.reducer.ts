import { CustomerAction, CustomerActionTypes } from "./customer.actions";
import { Customer } from "./customer.model";

export const CUSTOMER_FEATURE_KEY = "customer";

/**
 * Interface for the 'Customer' data used in
 *  - CustomerState, and
 *  - customerReducer
 *
 *  Note: replace if already defined in another module
 */


export interface CustomerState {
  list: Customer[]; // list of Customer; analogous to a sql normalized table
  selectedCustomerId?: string | null; // which Customer record has been selected
  loaded: boolean; // has the Customer list been loaded
  body?: Customer,
  error?: any; // last none error (if any)
}

export interface CustomerPartialState {
  readonly [CUSTOMER_FEATURE_KEY]: CustomerState;
}

export const initialState: CustomerState = {
  list: [],
  selectedCustomerId: null,
  body: null,
  loaded: false
};

export function customerReducer(
  state: CustomerState = initialState,
  action: CustomerAction
): CustomerState {
  let list;

  switch (action.type) {
    case CustomerActionTypes.CustomerLoaded: {

      state = {
        ...state,
        list: action.payload,
        loaded: true
      };
      break;
    }
    case CustomerActionTypes.UpdateCustomerSuccess: {
      state = {
        ...state,
        body: action.payload,
        loaded: true
      };
      break;
    }
    case CustomerActionTypes.GetCustomerById: {
      const selectedCustomerId = action.payload;
      state = {
        ...state,
        selectedCustomerId
      };
      break;
    }
    case CustomerActionTypes.SaveCustomerSuccessfully: {
      const items = [action.payload];
      list = items.reduce((acc, it) => {
        return addCustomer(it, acc);
      }, state.list);

      state = { ...state, list, loaded: true };
    }

  }
  return state;
}

/**
 * Add customer to existing list. If already existing,
 * overwrite with new values. Maintain customer order.
 */
function addCustomer(customer: Customer, list: Customer[]): Customer[] {
  const found = findExisting(customer, list);
  const result = !!found
    ? list.map(it => {
      return it.id === customer.id ? { ...it, ...customer } : it;
    })
    : list.concat(customer);

  return result;
}

/**
 * Find existing customer (if present)
 */
function findExisting(customer: Customer, buffer: Customer[]): Customer {
  return buffer.reduce((prev, curr): Customer => {
    return prev ? prev : curr.id === customer.id ? curr : null;
  }, null);
}
