import { Action } from "@ngrx/store";
import { Customer } from "./customer.model";

export enum CustomerActionTypes {
  LoadCustomer = "[Customer] Load Customer",
  CustomerLoaded = "[Customer] Customer Loaded",
  CustomerLoadError = "[Customer] Customer Load Error",
  SaveCustomer ="[Customer] Save Customer",
  SaveCustomerError ="[Customer] Save Customer Error",
  SaveCustomerSuccessfully ="[Customer] Save Customer Successfully",
  GetCustomerById = "[Customer] GetCustomerById",
  UpdateCustomer = "[Customer] UpdateCustomer",
  UpdateCustomerError = "[Customer] UpdateCustomerError",
  UpdateCustomerSuccess = "[Customer] UpdateCustomerSuccess",
}

export class LoadCustomer implements Action {
  readonly type = CustomerActionTypes.LoadCustomer;
}

export class CustomerLoadError implements Action {
  readonly type = CustomerActionTypes.CustomerLoadError;
  constructor(public payload: any) {}
}

export class CustomerLoaded implements Action {
  readonly type = CustomerActionTypes.CustomerLoaded;
  constructor(public payload: Customer[]) {}
}

export class SaveCustomer implements  Action {
  readonly type = CustomerActionTypes.SaveCustomer;
  constructor(public payload: Customer) {}
}

export class SaveCustomerError implements Action {
  readonly type = CustomerActionTypes.SaveCustomerError;
  constructor(public payload: Customer) {}
}

export class SaveCustomerSuccessfully implements  Action {
  readonly type = CustomerActionTypes.SaveCustomerSuccessfully;
  constructor(public payload: Customer) {}
}

export class GetCustomerById implements  Action {
  readonly type = CustomerActionTypes.GetCustomerById;
  constructor(public payload: string) {}
}

export class UpdateCustomer implements Action {
  readonly type = CustomerActionTypes.UpdateCustomer;

  constructor(public customerId: string, public body: Customer) {
  }

}

export class UpdateCustomerError implements Action {
  readonly type = CustomerActionTypes.UpdateCustomerError;

  constructor(public payload: string) {
  }

}

export class UpdateCustomerSuccess implements Action {
  readonly type = CustomerActionTypes.UpdateCustomerSuccess;

  constructor(public payload: Customer) {
  }

}

export type CustomerAction = LoadCustomer | CustomerLoaded | CustomerLoadError | SaveCustomer | SaveCustomerError | SaveCustomerSuccessfully | GetCustomerById
  | UpdateCustomer | UpdateCustomerError | UpdateCustomerSuccess;

export const fromCustomerActions = {
  LoadCustomer,
  CustomerLoaded,
  CustomerLoadError
};
