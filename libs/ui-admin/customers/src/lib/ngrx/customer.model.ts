
export interface Customer {
  id?: string
  fullName: string
  email: string
  phone: string
  address: string
  subject: string
  messages: string
}
