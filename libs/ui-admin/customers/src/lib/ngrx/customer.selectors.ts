import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CUSTOMER_FEATURE_KEY, CustomerState } from "./customer.reducer";

// Lookup the 'Customer' feature state managed by NgRx
const getCustomerState = createFeatureSelector<CustomerState>(
  CUSTOMER_FEATURE_KEY
);

const getLoaded = createSelector(
  getCustomerState,
  (state: CustomerState) => state.loaded
);
const getError = createSelector(
  getCustomerState,
  (state: CustomerState) => state.error
);

const getAllCustomer = createSelector(
  getCustomerState,
  getLoaded,
  (state: CustomerState, isLoaded) => {
    return isLoaded ? state.list : [];
  }
);
const getSelectedId = createSelector(
  getCustomerState,
  (state: CustomerState) => state.selectedCustomerId
);
const getSelectedCustomer = createSelector(
  getAllCustomer,
  getSelectedId,
  (customer, id) => {
    const result = customer.find(it => it["_id"] === id);
    return result ? Object.assign({}, result) : undefined;
  }
);

export const customerQuery = {
  getLoaded,
  getError,
  getAllCustomer,
  getSelectedCustomer
};
