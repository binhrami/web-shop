import { FormControl, FormGroup, Validators } from "@angular/forms";

export interface CreateCustomerForm {
  fullName: string
  email: string
  phone: string
  address: string
  subject: string
  messages: string
}

export const CreateCustomerForm = {
  create: () => {
    const form = new FormGroup({
      fullName: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required]),
      address: new FormControl(''),
      subject: new FormControl('', [Validators.required]),
      messages: new FormControl(''),
    })



    return form
  },
}
