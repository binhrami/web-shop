import { Component, Injectable, OnInit } from "@angular/core";
import { MatDialogRef } from "@angular/material";
import { CreateCustomerForm } from "./create-customer.form";
import { CustomerFacade } from "../ngrx/customer.facade";

@Injectable()
@Component({
  selector: "seed-dialog-create",
  styleUrls: ["dialog-create-customer.component.scss"],
  templateUrl: "dialog-create-customer.component.html"
})
export class DialogCreateCustomerComponent implements OnInit {
  formCreateCustomer = CreateCustomerForm.create();

  constructor(
    public dialogRef: MatDialogRef<DialogCreateCustomerComponent>,
    private customerFacade: CustomerFacade) {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  create() {
    this.dialogRef.close(this.formCreateCustomer.value);
    this.customerFacade.createCustomer(this.formCreateCustomer.value);



  }

  ngOnInit(): void {


  }
}
