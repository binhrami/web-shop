import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { LoginComponent } from "./login.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ChartsModule as Ng2Charts } from "ng2-charts";
import { RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import {
  LOGIN_FEATURE_KEY,
  initialState as loginInitialState,
  loginReducer
} from "./ngrx/login.reducer";
import { LoginEffects } from "./ngrx/login.effects";
import { LoginFacade } from "./ngrx/login.facade";

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    RouterModule.forChild([
      {
        path: "",
        component: LoginComponent
      }
    ]),
    Ng2Charts,
    MatButtonModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false }),
    StoreModule.forFeature(LOGIN_FEATURE_KEY, loginReducer, {
      initialState: loginInitialState
    }),
    EffectsModule.forFeature([LoginEffects])
  ],
  declarations: [LoginComponent],
  providers: [LoginFacade]
})
export class UiAdminLoginModule {}
