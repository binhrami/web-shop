import { FormControl, FormGroup } from "@angular/forms";

export interface Login {
  email: string;
  password: string;
}

export const LoginForm = {
  create(): FormGroup {
    return new FormGroup({
      email: new FormControl(""),
      password: new FormControl("")
    });
  }
};
