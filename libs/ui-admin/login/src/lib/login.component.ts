import { Component, OnInit } from "@angular/core";
import { LoginForm } from "./login.form";
import { BaseService, Helper } from "@seed/common/core";
import { Router } from '@angular/router';
// import Security  from "../../../../common/core/src/lib/Security";

@Component({
  selector: "seed-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  form = LoginForm.create();
  constructor(private http: BaseService, private route: Router, private hepler: Helper) {}

  ngOnInit() {}

  onLogin() {
    const { email, password } = this.form.value;
    const options = {
      email: email,
      password: password
    };
      // const backupInfo = Security.getSetBackupInfo();

        // email = this.isExpired ? backupInfo.username : data.email;


    this.http
      .login("/users/login", options)
      .subscribe(data => {
        this.hepler.updateInfoToStore();
        localStorage.setItem('isLoggedin', 'true');
         this.route.navigate(['/dashboard'])

      });
  }
}
