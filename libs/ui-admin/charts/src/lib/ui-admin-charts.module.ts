import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChartsComponent } from './charts/charts.component';
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [CommonModule,
  RouterModule.forChild([
    {
      path: '',
      component: ChartsComponent
    }
  ])],
  declarations: [ChartsComponent]
})
export class UiAdminChartsModule {}
