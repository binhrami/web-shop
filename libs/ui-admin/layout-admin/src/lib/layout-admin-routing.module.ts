import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutComponent } from "./layout/layout.component";


const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        redirectTo: "dashboard"
      },
      {
        path: "dashboard",
        loadChildren: "./layout/dashboard/dashboard.module#DashboardModule"
      },
      {
        path: "charts",
        loadChildren: "@seed/ui-admin/charts#UiAdminChartsModule"
      },
      {
        path: "blogs",
        loadChildren: "@seed/ui-admin/blogs#UiAdminBlogsModule"
      },
      {
        path: "services",
        loadChildren: "@seed/ui-admin/service-management#UiAdminServicesManagementModule"
      },
      {
        path: "customers",
        loadChildren: "@seed/ui-admin/customers#UiAdminCustomersModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutAdminRoutingModule {
}
