import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LayoutComponent } from "./layout/layout.component";
import { LayoutAdminRoutingModule } from "./layout-admin-routing.module";
import {
  MatButtonModule, MatCardModule, MatGridListModule, MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule
} from "@angular/material";
import { SidebarComponent } from "./layout/components/sidebar/sidebar.component";
import { TopnavComponent } from "./layout/components/topnav/topnav.component";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  imports: [CommonModule,
    LayoutAdminRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatListModule,
    MatCardModule, MatGridListModule,
    TranslateModule

  ],
  declarations: [LayoutComponent, TopnavComponent, SidebarComponent]
})
export class LayoutAdminModule {
}
