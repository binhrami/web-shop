import { Component, OnInit } from "@angular/core";
import { ServiceManagementFacade } from "../ngrx/service-management.facade";
import { Service } from "@seed/ui-admin/service-management";
import { Observable } from "rxjs";

@Component({
  selector: 'seed-services-manage',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  displayedColumns = ['title' ,'modified','createdBy'];
  dataSource$: Observable<Service[]> = this.serviceFacade.allServiceManagement$;

  constructor(private serviceFacade: ServiceManagementFacade) {
    this.serviceFacade.getListService();
  }

  ngOnInit() {
  }

}
