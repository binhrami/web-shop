import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ServiceManagementFacade } from "../ngrx/service-management.facade";
import { Observable } from "rxjs";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Service, ServiceContent } from "@seed/ui-admin/service-management";
import { MatDialog } from "@angular/material";
import { DialogImagesComponent } from "../dialog/dialog-images.component";
import { ToastrService } from "ngx-toastr";


@Component({
  selector: 'seed-services-details',
  templateUrl: './services-details.component.html',
  styleUrls: ['./services-details.component.scss']
})
export class ServicesDetailsComponent implements OnInit {
  service$: Observable<Service> = this.serviceFacade.selectedServiceManagement$;
  name = 'ng2-ckeditor';
  ckeConfig: any;
  idService: string;
  log = '';
  @ViewChild("myckeditor") ckeditor: any;
  serviceForm : FormGroup;
  content: string;
  enabledForm: true;

  constructor(private toastService: ToastrService, private route: ActivatedRoute, private serviceFacade: ServiceManagementFacade, private fb: FormBuilder, private router: Router,
              private dialog: MatDialog) {
    this.fb.group({});
    this.idService = this.route.snapshot.params['id'];
    this.serviceFacade.getServiceById(this.idService);
    this.serviceForm = this.fb.group({
      title: [''],
      images: [''],
    });
  }

  ngOnInit() {
    this.serviceForm.valueChanges.subscribe(() => {
      this.enabledForm = true;
    });
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: 'divarea',
      forcePasteAsPlainText: true
    };
  }

  onChange($event: any): void {
    this.content = $event;
    this.enabledForm = true;
  }

  updateImagesDialog(images: string, idService: string) {
    const dialogRef = this.dialog.open(DialogImagesComponent, {
      disableClose: true,
      width: "1000px",
      data:  {
        images: images,
        idService: idService
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.serviceFacade.getListService();
      this.toastService.success( 'Cập nhật hình ảnh thành công !' )
      if (result) {
      }
    });
  }



  saveContent() {
    const body: ServiceContent = {
      content: this.content
    };
    this.serviceFacade.updateServiceById(this.idService, body);
    this.router.navigate(['/services']);
    this.toastService.success('Cập nhật dịch vụ thành công !')

  }

}
