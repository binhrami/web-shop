import { Action } from "@ngrx/store";
import { Service, ServiceContent } from "@seed/ui-admin/service-management";

export enum ServiceManagementActionTypes {
  LoadServiceManagement = "[ServiceManagement] Load ServiceManagement",
  ServiceManagementLoaded = "[ServiceManagement] ServiceManagement Loaded",
  ServiceManagementLoadError = "[ServiceManagement] ServiceManagement Load Error",
  GetServiceById = "[ServiceManagement] GetServiceById",
  UpdateService = "[ServiceManagement] UpdateService",
  UpdateServiceError = "[ServiceManagement] UpdateServiceError",
  UpdateServiceSuccess = "[ServiceManagement] UpdateServiceSuccess",
}

export class LoadServiceManagement implements Action {
  readonly type = ServiceManagementActionTypes.LoadServiceManagement;
}

export class ServiceManagementLoadError implements Action {
  readonly type = ServiceManagementActionTypes.ServiceManagementLoadError;

  constructor(public payload: any) {
  }
}

export class ServiceManagementLoaded implements Action {
  readonly type = ServiceManagementActionTypes.ServiceManagementLoaded;

  constructor(public payload: Service[]) {
  }
}

export class GetServiceById implements Action {
  readonly type = ServiceManagementActionTypes.GetServiceById;

  constructor(public payload: string) {
  }

}

export class UpdateService implements Action {
  readonly type = ServiceManagementActionTypes.UpdateService;

  constructor(public serviceId: string, public content: ServiceContent) {
  }

}

export class UpdateServiceError implements Action {
  readonly type = ServiceManagementActionTypes.UpdateServiceError;

  constructor(public payload: string) {
  }

}

export class UpdateServiceSuccess implements Action {
  readonly type = ServiceManagementActionTypes.UpdateServiceSuccess;

  constructor(public payload: Service) {
  }

}


export type ServiceManagementAction =
  | LoadServiceManagement
  | ServiceManagementLoaded
  | ServiceManagementLoadError  | GetServiceById | UpdateService | UpdateServiceError | UpdateServiceSuccess ;

export const fromServiceManagementActions = {
  LoadServiceManagement,
  ServiceManagementLoaded,
  ServiceManagementLoadError,
  GetServiceById,
};
