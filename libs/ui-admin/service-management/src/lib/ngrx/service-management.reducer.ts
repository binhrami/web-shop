import {
  ServiceManagementAction,
  ServiceManagementActionTypes
} from "./service-management.actions";

export const SERVICEMANAGEMENT_FEATURE_KEY = "serviceManagement";

/**
 * Interface for the 'ServiceManagement' data used in
 *  - ServiceManagementState, and
 *  - serviceManagementReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Service {
  _id: string;
  title: string;
  content: string;
  modified: string;
  images: string;
  createdBy: string
  created: string
}
export interface  ServiceContent {
  images?: string,
  content?: string
}
export interface ServiceManagementState {
  list: Array<Service>; // list of ServiceManagement; analogous to a sql normalized table
  selectedServiceId: string | null; // which ServiceManagement record has been selected
  loaded: boolean; // has the ServiceManagement list been loaded
  error?: any; // last none error (if any)
  content?: Service,

}

export interface ServiceManagementPartialState {
  readonly [SERVICEMANAGEMENT_FEATURE_KEY]: ServiceManagementState;
}

export const initialState: ServiceManagementState = {
  list: [],
  selectedServiceId: null,
  content: null,
  loaded: false
};

export function serviceManagementReducer(
  state: ServiceManagementState = initialState,
  action: ServiceManagementAction
): ServiceManagementState {
  switch (action.type) {
    case ServiceManagementActionTypes.ServiceManagementLoaded: {
      state = {
        ...state,
        list: action.payload,
        loaded: true
      };
      break;
    }
    case ServiceManagementActionTypes.GetServiceById: {
      const selectedServiceId = action.payload;
      state = {
        ...state,
        selectedServiceId
      };
      break;
    }
    case ServiceManagementActionTypes.UpdateServiceSuccess: {
      state = {
        ...state,
        content: action.payload,
        loaded: true
      };
      break;
    }
  }
  return state;
}
