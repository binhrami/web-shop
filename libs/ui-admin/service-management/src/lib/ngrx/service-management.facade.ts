import { Injectable } from "@angular/core";

import { select, Store } from "@ngrx/store";

import { ServiceManagementQuery } from "./service-management.selectors";
import {
  GetServiceById,
  LoadServiceManagement, UpdateService
} from "./service-management.actions";
import { ServiceContent, ServiceManagementPartialState } from "@seed/ui-admin/service-management";

@Injectable()
export class ServiceManagementFacade {
  loaded$ = this.store.pipe(select(ServiceManagementQuery.getLoaded));
  allServiceManagement$ = this.store.pipe(
    select(ServiceManagementQuery.getAllServiceManagement)
  );

  selectedServiceManagement$ = this.store.pipe(
    select(ServiceManagementQuery.getSelectedServiceManagement)
  );

  constructor(private store: Store<ServiceManagementPartialState>) {}

  getListService() {
    this.store.dispatch(new LoadServiceManagement());
  }

  getServiceById(serviceId: string) {
    this.store.dispatch(new GetServiceById(serviceId));
  }

  updateServiceById(serviceId: string, content: ServiceContent) {
    this.store.dispatch(new UpdateService(serviceId, content));
  }
}
