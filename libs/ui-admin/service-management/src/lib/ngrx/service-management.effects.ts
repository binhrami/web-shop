import { Injectable } from "@angular/core";
import { Effect, Actions } from "@ngrx/effects";
import { DataPersistence } from "@nrwl/nx";

import { Service, ServiceManagementPartialState } from "./service-management.reducer";
import {
  LoadServiceManagement,
  ServiceManagementLoaded,
  ServiceManagementLoadError,
  ServiceManagementActionTypes, UpdateService, UpdateServiceError, UpdateServiceSuccess
} from "./service-management.actions";
import { BaseService } from "@seed/common/core";
import { map } from "rxjs/operators";

@Injectable()
export class ServiceManagementEffects {
  @Effect() loadServiceManagement$ = this.dataPersistence.fetch(
    ServiceManagementActionTypes.LoadServiceManagement,
    {
      run: (action: LoadServiceManagement, state: ServiceManagementPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        // return new ServiceManagementLoaded([]);
        return this.http.get("/service").pipe(map((res: Service[]) =>  new ServiceManagementLoaded(res) ))
      },

      onError: (action: LoadServiceManagement, error) => {
        console.error("Error", error);
        return new ServiceManagementLoadError(error);
      }
    }
  );

  @Effect() updateService$ = this.dataPersistence.fetch(
    ServiceManagementActionTypes.UpdateService,
    {
      run: (action: UpdateService, state: ServiceManagementPartialState) => {
        // Your custom REST 'load' logic goes here. For now just return an empty list...
        // return new ServiceManagementLoaded([]);

        return this.http.put(`/service/${action.serviceId}`, action.content).pipe(map((res: any) =>  new UpdateServiceSuccess(res) ))
      },

      onError: (action: UpdateService, error) => {
        console.error("Error", error);
        return new UpdateServiceError(error);
      }
    }
  );

  constructor(
    private actions$: Actions,
    private http: BaseService,
    private dataPersistence: DataPersistence<ServiceManagementPartialState>
  ) {}
}
