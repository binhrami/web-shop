import { createFeatureSelector, createSelector } from "@ngrx/store";
import {
  SERVICEMANAGEMENT_FEATURE_KEY,
  ServiceManagementState
} from "./service-management.reducer";

// Lookup the 'ServiceManagement' feature state managed by NgRx
const getServiceManagementState = createFeatureSelector<ServiceManagementState>(
  SERVICEMANAGEMENT_FEATURE_KEY
);

const getLoaded = createSelector(
  getServiceManagementState,
  (state: ServiceManagementState) => state.loaded
);
const getError = createSelector(
  getServiceManagementState,
  (state: ServiceManagementState) => state.error
);

const getAllServiceManagement = createSelector(
  getServiceManagementState,
  getLoaded,
  (state: ServiceManagementState, isLoaded) => {
    return isLoaded ? state.list : [];
  }
);
const getSelectedId = createSelector(
  getServiceManagementState,
  (state: ServiceManagementState) => state.selectedServiceId
);
const getSelectedServiceManagement = createSelector(
  getAllServiceManagement,
  getSelectedId,
  (ServiceManagement, id) => {
    const result = ServiceManagement.find(service => service._id === id);

    // when object from service have format [9 , [{}]]
    // let result = null;
    // ServiceManagement.some(item => {
    //   if (Array.isArray(item)) {
    //     result = item.find(i => i._id === id);
    //     return result != null;
    //   }
    // });
    return result ? Object.assign({}, result) : undefined;
  }
);

export const ServiceManagementQuery = {
  getLoaded,
  getError,
  getSelectedId,
  getAllServiceManagement,
  getSelectedServiceManagement
};
