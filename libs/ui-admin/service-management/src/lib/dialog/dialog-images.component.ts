import { Component, Inject, Injectable, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { ServiceManagementFacade } from "../ngrx/service-management.facade";
import { ServiceContent } from "@seed/ui-admin/service-management";
import { FormBuilder, FormGroup } from "@angular/forms";

@Injectable()
@Component({
  selector: "seed-dialog-images",
  styleUrls: ["dialog-imagescomponent.scss"],
  templateUrl: "dialog-images.component.html"
})
export class DialogImagesComponent implements OnInit {
  serviceForm : FormGroup;

  constructor(
    private fb: FormBuilder,
    private serviceFacade: ServiceManagementFacade,
    public dialogRef: MatDialogRef<DialogImagesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.serviceForm = this.fb.group({
      images: [''],
    });

  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  updateImagesMain(idService: string) {
    const body: ServiceContent = {
      images: this.serviceForm.controls.images.value
    };
    this.serviceFacade.updateServiceById(idService, body);
  }

  ngOnInit(): void {


  }
}
