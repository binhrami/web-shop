import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ServicesComponent } from "./services/services.component";
import { RouterModule } from "@angular/router";
import { CKEditorModule } from "ng2-ckeditor";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatButtonModule,
  MatCardModule, MatDialogModule, MatFormFieldModule,
  MatGridListModule,
  MatIconModule, MatInputModule,
  MatTableModule
} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ServicesDetailsComponent } from "./services-detail/services-details.component";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import {
  SERVICEMANAGEMENT_FEATURE_KEY,
  initialState as ServiceManagementInitialState,
  serviceManagementReducer
} from "./ngrx/service-management.reducer";
import { ServiceManagementEffects } from "./ngrx/service-management.effects";
import { ServiceManagementFacade } from "./ngrx/service-management.facade";
import { TranslateModule } from "@ngx-translate/core";
import { DialogImagesComponent } from "./dialog/dialog-images.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CKEditorModule,
    FormsModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    TranslateModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false }),
    RouterModule.forChild([
      {
        path: "",
        component: ServicesComponent
      },
      {
        path: ":id",
        component: ServicesDetailsComponent
      }
    ]),
    StoreModule.forFeature(SERVICEMANAGEMENT_FEATURE_KEY, serviceManagementReducer, {
      initialState: ServiceManagementInitialState
    }),
    EffectsModule.forFeature([ServiceManagementEffects])
  ],
  declarations: [ServicesComponent, ServicesDetailsComponent, DialogImagesComponent],
  entryComponents: [DialogImagesComponent],
  providers: [ServiceManagementFacade]
})
export class UiAdminServicesManagementModule {}
