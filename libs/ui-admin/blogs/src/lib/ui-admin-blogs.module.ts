import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BlogManagementComponent } from "./blog-management/blog-management.component";
import { UpdateBlogComponent } from "./update-blog/update-blog.component";
import { DialogComponent } from "./dialog/dialog.component";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import {
  BLOG_FEATURE_KEY,
  initialState as blogInitialState,
  blogReducer
} from "./ngrx/blog.reducer";
import { BlogEffects } from "./ngrx/blog.effects";
import { BlogFacade } from "./ngrx/blog.facade";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatGridListModule, MatIconModule,
  MatInputModule,
  MatTableModule
} from "@angular/material";
import { TranslateModule } from "@ngx-translate/core";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatGridListModule,
    MatCardModule,
    MatFormFieldModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    TranslateModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false }),
    StoreModule.forFeature(BLOG_FEATURE_KEY, blogReducer, {
      initialState: blogInitialState
    }),
    RouterModule.forChild([
      {
        path: "",
        component: BlogManagementComponent
      },
      {
        path: ":id",
        component: UpdateBlogComponent
      }
    ]),
    EffectsModule.forFeature([BlogEffects])
  ],
  declarations: [BlogManagementComponent, UpdateBlogComponent, DialogComponent],
  providers: [BlogFacade]
})
export class UiAdminBlogsModule {}
