import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import Security from "../Security";
import { UserStore } from "../UserStore";
import * as _ from 'lodash'
import { Injectable } from "@angular/core";

@Injectable()
export class HandlerInterceptors implements HttpInterceptor {
  constructor(private userStore: UserStore) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const options = req.body || {};
    let headers, token;

    if (options.email && options.password) {
      //Hash password
      const password = Security.hashPassword(options.email, options.password);
      headers = Security.getHeader(
        req.method,
        options.endPoint,
        options.email,
        password,
        options
      );
      token = options.email;

    } else {
      const secret = options.token ? "" : this.userStore.getUserInfo("secret");
      token = options.token || this.userStore.getUserInfo("token");
      headers = Security.getHeader(req.method, req.url, token, secret, options.body);

    }
    headers["Authorization"] = "Bearer " + token;
    headers["Client-Request"] = true;

    if (options.params && options.params.indexOf("token") !== -1) {
      _.unset(headers, "Authorization");
    }


    req = req.clone({
      setHeaders: headers
    });

    return next.handle(req)
  }

}
