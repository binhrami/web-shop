import { ModuleWithProviders, NgModule } from "@angular/core";
import { BaseService,  } from "./base.service";
import { HandlerInterceptors} from "./interceptors";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { UserStore } from "../UserStore";


@NgModule({
})
export class ApiModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        BaseService,
        UserStore,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HandlerInterceptors,
          multi: true
        }
      ]
    }
  }
}
