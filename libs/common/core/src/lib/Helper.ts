import * as _ from "lodash";
import { Injectable } from "@angular/core";
import { AccountStore } from "./AccountStore";
import { UserStore } from "./UserStore";
declare const $: any;

@Injectable()
export class Helper {
  constructor(private accountStore: AccountStore, private userStore: UserStore) {

  }

  /**
   * Get full option of magnific popup
   * @param options
   * @returns options of magnific popup
   */
  public static getOptionsMagnificPopup(options?: any): any {
    const opt = {
        type: "inline",
        preloader: false,
        prependTo: "#popupWrap"
      },
      defaultCallbacks = {
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        beforeOpen: function() {
          if ($(window).width() < 700) {
            this.st.focus = false;
          } else {
            this.st.focus = options && options.focus;
          }
          if (options && options.callbacks && options.callbacks.beforeOpen) {
            options.callbacks.beforeOpen();
          }
        },
        open: function() {},
        change: function() {
          _.set(this, "modalChanged", true);
          setTimeout(() => {
            _.set(this, "modalChanged", false);
          }, 100);
          const name = this.content.attr("id"),
            optionsModal = this.optionsModal && this.optionsModal[name],
            popupClass = this.content.attr("class") || "";
          if (
            optionsModal &&
            optionsModal.callbacks &&
            optionsModal.callbacks.open
          ) {
            optionsModal.callbacks.open();
          }
          if (this.openingModals[this.openingModals.length - 1] !== name) {
            this.openingModals.push(name);
          }

          //Which size popup to open based on class name on a tag
          this.wrap.removeClass("md-popup sm-popup xl-popup lg-popup");
          if (popupClass.indexOf("md-popup") >= 0) {
            this.wrap.addClass("md-popup");
          } else if (popupClass.indexOf("sm-popup") >= 0) {
            this.wrap.addClass("sm-popup");
          } else if (popupClass.indexOf("xl-popup") >= 0) {
            this.wrap.addClass("xl-popup");
          } else {
            this.wrap.addClass("lg-popup");
          }
        },
        close: function() {}
      };
    _.assign(opt, options || {});
    _.set(opt, "callbacks", defaultCallbacks);

    return opt;
  }
  public static openMagnificPopup(options, id) {
    const opt = Helper.getOptionsMagnificPopup(options),
      magIns = $.magnificPopup.instance,
      openingModalId = <any>_.last(magIns.openingModals);
    this.setInstanceCallback(id, options);

    //If the opening modal is the top modal, then push it into queue openingModal before the last
    if (
      openingModalId &&
      magIns &&
      magIns.optionsModal[openingModalId] &&
      magIns.optionsModal[openingModalId].topModal
    ) {
      magIns.openingModals.splice(magIns.openingModals.length - 2, 0, id);
    } else {
      //If the opening modal is not the top modal or nothing opening, then open it
      magIns.open(opt, 0);
    }
  }

  private static setInstanceCallback(name, optionsModal) {
    if (!$.magnificPopup.instance.optionsModal) {
      $.magnificPopup.instance.optionsModal = {};
    }
    if (!$.magnificPopup.instance.openingModals) {
      $.magnificPopup.instance.openingModals = [];
    }
    $.magnificPopup.instance.optionsModal[name] = optionsModal || {};
  }

  /**
   * close Magnific popup
   */
  public static closeMagnificPopup() {
    $.magnificPopup.close();
  }
  public updateInfoToStore() {
    this.accountStore.updateLoginAccountToStore();
    this.userStore.updateLoginUserToStore();
  }
}
