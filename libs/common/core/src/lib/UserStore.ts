import Storage from "./Storage";
import { USER_INFO } from "./Constant";


export class UserStore {
  userIdLogin: string;

  constructor() {}

  getUserInfo(configName?: string) {
    let userInfo = Storage.get(USER_INFO.ALL) || "";

    try {
      userInfo = JSON.parse(userInfo);
    } catch (err) {
      userInfo = "";
    }

    return configName && userInfo ? userInfo[configName] : userInfo;
  }
  updateLoginUserToStore() {
    this.userIdLogin = this.getUserInfo(USER_INFO.USER_ID) || null;
  }
}
