import {EventEmitter} from "@angular/core";

import Storage from ".//Storage";
import { ACCOUNT_INFO } from "./Constant";


export class AccountStore extends EventEmitter<any> {
  accIdLogin: string;

  constructor() {
    super();


  }

  getAccountInfo(configName?) {
    let accountInfo = Storage.get('accountInfo');

    try {
      accountInfo = JSON.parse(accountInfo);
    }
    catch (err) {
      accountInfo = "";
    }

    return configName && accountInfo ? accountInfo[configName] : accountInfo;
  }

  updateLoginAccountToStore() {
    this.accIdLogin = this.getAccountInfo(ACCOUNT_INFO.ID) || null;
  }

}
