export class Storage {
  constructor() {}

  get(key: string) {
    return localStorage.getItem(key);
  }

  set(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  clear() {
    localStorage.clear();
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }

  setGetRemoveReloadInfo(
    set: boolean = false,
    get: boolean = false,
    remove: boolean = false
  ) {
    const key = "reload",
      value = "true";

    if (set) {
      this.set(key, value);
    } else if (get) {
      return this.get(key) === value;
    } else if (remove) {
      this.remove(key);
    }
  }
}

const storage = new Storage();
export default storage;
