export const STORAGE_KEYS = {
  ALL_FEATURES: "allFeatures",
  ACCOUNT_FEATURES: "featuresIncludePermission",
  ACCOUNT_CRITERIA: "accountCriteria",
  AUTOMATION_CRITERIA: "automationCriteria",
  CHANNELS_CRITERIA: "channelsCriteria",
  CAMPAIGNS_CRITERIA: "campaignCriteria",
  CAMPAIGNS_DETAILS_CRITERIA: "campaignDetailsCriteria",
  CLIENTPACKAGE_CRITERIA: "clientPackageCriteria",
  CONTACT_CRITERIA: "contactCriteria",
  CONTACT_DATABASE_COLUMNS: "contactDatabaseColumns",
  INBOX_CRITERIA: "inboxCriteria",
  ROLE_CRITERIA: "roleCriteria",
  USER_CRITERIA: "userCriteria",
  WEBFORM_CRITERIA: "webformCriteria",
  WHITE_LABEL_STYLES: "whitelabelStyles",
  WEBFORM_SUBMISSION_CRITERIA: "webformSubmissionCriteria",
  REGISTERED_APP_CRITERIA: "registeredAppCriteria",
  ACCOUNT_ACTIVITY_CRITERIA: "accountActivityCriteria"
};

export const HTTP_VERBS = {
  PUT: "PUT",
  POST: "POST",
  GET: "GET",
  DELETE: "DELETE",
  HEAD: "HEAD"
};

export const CONFIG_KEYS = {
  API_DOMAIN: "apiDomain",
  FRONT_END_GA: "frontendGa",
  FRONT_END_GA_TRACKING_ID: "frontendGaTrackingId",
  FRONTEND_SENTRY_DSN: "frontendSentryDSN",
  SUB_DOMAIN: "subDomain",
  APPCUES_ENABLED: "appcuesEnabled",
  APPCUES_ID: "appcuesId"
};

export const ACCOUNT_INFO = {
  ID: "_id",
  NAME: "name",
  PARENT: "parent",
  PARENT_PATH: "parentPath",
  LEVEL: "level",
  BILLING: "billing",
  BILLING_TYPE: "billingType",
  ACCOUNT_TYPE: "accountType",
  PAY_TYPE: "payType",
  EMAIL_FEE: "emailFee",
  SMS_FEE: "smsFee",
  EMAIL_CREDITS: "currentPrepaidEmails",
  SMS_CREDITS: "currentPrepaidSms",
  MONTHLY_SUB_FEE: "monthlySubFee",
  CURRENT_INCLUDED_EMAILS: "currentIncludedEmails",
  CURRENT_INCLUDED_SMS: "currentIncludedSms",
  DEFAULT_CURRENCY: "billing.defaultCurrency",
  FEATURES: "features",
  STATUS: "status",
  WHITELABEL_SETUP: "whitelabelSetup",
  TIMEZONE: "timezone",
  SUB_DOMAIN: "subDomain"
};


export const USER_INFO = {
  ALL: "userInfo",
  USER_ID: "_id",
  ACCESS: "access",
  WHITELABEL: "whitelabel",
  ACCESS_CHILD: "accessChild",
  TOKEN: "token",
  LANGUAGE: "language",
  SECRET: "secret",
  EMAIL: "email",
  PHONE: "phone",
  IS_BRAND_USER: "isBrandUser",
  SETTINGS: "settings",
  USER_PROFILE: "userProfile",
  FIRST_NAME: "firstName",
  IS_SUPER_ADMIN: "isSuperAdmin"
};
