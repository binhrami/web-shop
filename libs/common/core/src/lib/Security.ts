import Storage from "./Storage";
import { STORAGE_KEYS } from "./Constant";
import CryptoJS from "crypto-js";
import urlencode from "./urlencode";
import format from "string-format";
import { sha256 } from "js-sha256";
export interface InfoUser {
  username: string;
  userId: string;
  accountId: string;
}

 class Security {
  private BACKUP_INFO = "backupInfo";

  constructor() {}

  setLogin(isLogin) {
    Storage.set("login", isLogin);
  }

  getLogin() {
    const isLogin = Storage.get("login");

    return isLogin ? isLogin : "";
  }

  clearLocalStore() {
    const backupInfo = Storage.get(this.BACKUP_INFO),
      whitelabelStyles = Storage.get(STORAGE_KEYS.WHITE_LABEL_STYLES),
      emailLogin = Storage.get("emailUser");

    Storage.clear();

    Storage.set(STORAGE_KEYS.WHITE_LABEL_STYLES, whitelabelStyles);
    Storage.set(this.BACKUP_INFO, backupInfo);
    if (emailLogin) {
      Storage.set("emailUser", emailLogin);
    }
  }

  /**
   * Check user logs in with a new set of credentials.
   * @param userId
   * @param accId
   * @returns {boolean}
   */
  isDiffUserOrAcc(userId, accId) {
    if (!userId || !accId) {
      return true;
    }

    const backupInfo = this.getSetBackupInfo();
    return backupInfo.userId !== userId || backupInfo.accountId !== accId;
  }

  /**
   * Set or get username, accountId, userId to localStorage
   * @param data: {username?: "", userId?: "", accountId?: ""}
   * @returns {any|{}}
   */
  getSetBackupInfo(data?: any) {
    let backupInfo = <any>Storage.get(this.BACKUP_INFO);

    backupInfo = JSON.parse(backupInfo) || {};

    if (data) {
      backupInfo.username = data.username ? data.username : backupInfo.username;
      backupInfo.userId = data.userId ? data.userId : backupInfo.userId;
      backupInfo.accountId = data.accountId
        ? data.accountId
        : backupInfo.accountId;

      Storage.set(this.BACKUP_INFO, JSON.stringify(backupInfo));
    } else {
      return backupInfo || {};
    }
  }

  /**
   * Hash password
   * @param {string} email
   * @param {string} password
   * @returns {any}
   */
  hashPassword(email: string, password: string): any {
    if (!(password && email)) {
      return true;
    }

    return sha256(email + sha256(password));
  }

  //With login token is email, secret is hashPassword
  getHeader(method, path, token, secret, body) {
    try {
      body = JSON.stringify(ksort(body));
    } catch (error) {
      body = "{}";
    }

    const nonce = getNonce();
    const timestamp = Math.floor(Date.now() / 1000);
    const signature = generateSignature();

    return {
      Signature: signature,
      Timestamp: timestamp,
      Nonce: nonce
    };

    function getNonce() {
      //Random string with 18 character
      const strRandom = Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 18);

      //Get date with format YYYYMMDDHHMMSS
      const date = new Date();
      const dateString = date
        .toISOString()
        .split(".")[0]
        .replace(/[-T:]/g, "");

      //Return nonce = string + date
      return strRandom + dateString;
    }

    // Create a signature by Method (PUT) & URL Path & Timestamp & Nonce & Body
    function generateSignature() {
      const arrayPath = path.split("?");
      const baseString = format(
        "{0}&{1}&{2}&{3}&{4}",
        urlencode.encode(method),
        urlencode.encode(arrayPath[0]),
        urlencode.encode(timestamp),
        urlencode.encode(nonce),
        urlencode.encode(body)
      );
      const key = token + "&" + secret;
      const sign = CryptoJS.enc.Base64.stringify(
        CryptoJS.HmacSHA1(baseString, key)
      );

      return sign;
    }

    function ksort(obj) {
      const keys = Object.keys(obj).sort(),
        sortedObj = {};

      for (const i in keys) {
        if (keys.hasOwnProperty(i) && obj[keys[i]] !== null) {
          if (
            Object.prototype.toString.call(obj[keys[i]]) === "[object Date]"
          ) {
            obj[keys[i]] = obj[keys[i]].toISOString();
          } else if (
            Object.prototype.toString.call(obj[keys[i]]) === "[object Array]" ||
            typeof obj[keys[i]] === "object"
          ) {
            obj[keys[i]] = ksort(obj[keys[i]]);
          }

          sortedObj[keys[i]] = obj[keys[i]];
        }
      }
      return sortedObj;
    }
  }


}

const security = new Security();
export default security;
