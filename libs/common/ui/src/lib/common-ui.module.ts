import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FooterComponent } from "./header/footer.component";
import { ScrollTopComponent } from "./scroll-top/scroll-top.component";
import { YourCartComponent } from "./your-cart/your-cart.component";
import { FillerAndSearchComponent } from "./filler-and-search/filler-and-search.component";
import { AdminBarComponent } from './admin-bar/admin-bar.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    FooterComponent,
    ScrollTopComponent,
    YourCartComponent,
    FillerAndSearchComponent,
    AdminBarComponent,
    MenuBarComponent
  ],
  exports: [
    FooterComponent,
    ScrollTopComponent,
    YourCartComponent,
    FillerAndSearchComponent,
    AdminBarComponent,
    MenuBarComponent
  ]
})
export class CommonUiModule {}
