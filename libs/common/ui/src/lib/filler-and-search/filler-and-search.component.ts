import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "seed-filler-and-search",
  templateUrl: "./filler-and-search.component.html",
  styleUrls: ["./filler-and-search.component.scss"]
})
export class FillerAndSearchComponent implements OnInit {
  @Output() isOpenFiller: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isOpenSearch: EventEmitter<boolean> = new EventEmitter<boolean>();
  isSearch = false;
  isFilter = false;

  ngOnInit() {}

  openSearch() {
    this.isSearch = !this.isSearch;
    this.isOpenSearch.emit(this.isSearch);
    this.isFilter = false;
  }

  openFilter() {
    this.isFilter = !this.isFilter;
    this.isOpenFiller.emit(this.isFilter);
    this.isSearch = false;
  }
}
