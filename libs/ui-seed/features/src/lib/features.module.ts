import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FeaturesComponent } from "./features.component";

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([{ path: "", component: FeaturesComponent }])
  ],
  declarations: [FeaturesComponent]
})
export class FeaturesModule {}
