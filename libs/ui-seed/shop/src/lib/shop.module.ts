import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ShopRoutingModule } from "./shop-routing.module";
import { ShopComponent } from "./shop.component";
import { CommonUiModule } from "@seed/common/ui";

@NgModule({
  imports: [CommonModule, ShopRoutingModule, CommonUiModule],
  declarations: [ShopComponent]
})
export class ShopModule {}
