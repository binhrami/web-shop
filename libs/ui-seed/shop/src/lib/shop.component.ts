import { Component, OnInit } from "@angular/core";

@Component({
  selector: "seed-homepage",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"]
})
export class ShopComponent implements OnInit {
  constructor() {}

  toggleFilter(value: any) {
    console.log(value);
  }
  toggleSearch(value: any) {
    console.log(value);
  }
  ngOnInit() {}
}
