import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BlogComponent } from "./blog.component";

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild([
      { path: "", pathMatch: "full", component: BlogComponent }
    ])
  ],
  declarations: [BlogComponent]
})
export class BlogModule {}
