import { Component, OnInit, Inject, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/common";

@Component({
  selector: "seed-home-layout",
  templateUrl: "./home-layout.component.html",
  styleUrls: ["./home-layout.component.scss"]
})
export class HomeLayoutComponent implements OnInit {
  windowScrolled: boolean;
  // client = document.getElementById('myDiv').clientHeight;
  constructor(@Inject(DOCUMENT) private document: Document) {}
  @HostListener("window:scroll", [])

  // constructor() { }
  onWindowScroll() {
    if (
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop > 100
    ) {
      this.windowScrolled = true;
    } else if (
      (this.windowScrolled && window.pageYOffset) ||
      document.documentElement.scrollTop ||
      document.body.scrollTop < 10
    ) {
      this.windowScrolled = false;
    }
  }

  ngOnInit() {}
}
