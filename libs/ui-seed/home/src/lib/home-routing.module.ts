import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { HomeLayoutComponent } from "./home-layout/home-layout.component";

const routes: Routes = [
  {
    path: "",
    component: HomeLayoutComponent,
    children: [
      {
        path: "",
        component: HomeComponent
      },
      {
        path: "shop",
        loadChildren: "@seed/ui-seed/shop#ShopModule"
      },
      {
        path: "features",
        loadChildren: "@seed/ui-seed/features#FeaturesModule"
      },
      {
        path: "blog",
        loadChildren: "@seed/ui-seed/blog#BlogModule"
      },
      {
        path: "about",
        loadChildren: "@seed/ui-seed/about#AboutModule"
      },
      {
        path: "contact",
        loadChildren: "@seed/ui-seed/contact#ContactModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
