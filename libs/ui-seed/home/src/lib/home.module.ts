import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeComponent } from "./home.component";
import { HomeRoutingModule } from "./home-routing.module";
import { CommonUiModule } from "@seed/common/ui";
import { HomeLayoutComponent } from "./home-layout/home-layout.component";
import { SlideshowModule } from "ng-simple-slideshow";
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { ImageViewerModule } from 'ng2-image-viewer';

@NgModule({
  imports: [
    CommonModule,
    CommonUiModule,
    HomeRoutingModule,
    SlideshowModule,
    SlickCarouselModule,
    ImageViewerModule
  ],
  declarations: [HomeComponent, HomeLayoutComponent],
})
export class HomeModule {}
