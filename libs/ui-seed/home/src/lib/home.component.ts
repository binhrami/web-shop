import { Component, OnInit } from "@angular/core";
import { IImage } from "ng-simple-slideshow";
@Component({
  selector: "seed-homepage",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  imageUrls: (string | IImage)[] = [
    { url: "./assets/images/slide-01.jpg" },
    { url: "./assets/images/slide-02.jpg" },
    { url: "./assets/images/slide-03.jpg" }
  ];
  imageHomes = [
    { id :0, title:"", image: "assets/home/images/web1/1.PNG", imageDetails: ["assets/home/images/web1/2.PNG","assets/home/images/web1/3.PNG","assets/home/images/web1/4.PNG"] },
    { id :1, title:"", image: "assets/home/images/web2/1.PNG", imageDetails: ["assets/home/images/web2/2.PNG","assets/home/images/web2/3.PNG","assets/home/images/web2/4.PNG"] },
    { id :2, title:"", image: "assets/home/images/web3/1.png", imageDetails: ["assets/home/images/web3/2.png","assets/home/images/web3/3.png","assets/home/images/web3/4.png"] },
    { id :3, title:"", image: "assets/home/images/web4/1.png", imageDetails: ["assets/home/images/web4/2.png","assets/home/images/web4/3.png","assets/home/images/web4/4.png"] },
    { id :4, title:"", image: "assets/home/images/web5/1.png", imageDetails: ["assets/home/images/web5/2.png","assets/home/images/web5/3.png","assets/home/images/web5/4.png"] },
    { id :5, title:"", image: "assets/home/images/web6/1.png", imageDetails: ["assets/home/images/web6/2.png","assets/home/images/web6/3.png","assets/home/images/web6/4.png"] },
    { id :6, title:"", image: "assets/home/images/web7/1.png", imageDetails: ["assets/home/images/web7/2.png","assets/home/images/web7/3.png","assets/home/images/web7/4.png"] },
    { id :7, title:"", image: "assets/home/images/web8/1.png", imageDetails: ["assets/home/images/web8/2.png","assets/home/images/web8/3.png","assets/home/images/web8/4.png"] },
    { id :8, title:"", image: "assets/home/images/web9/1.png", imageDetails: ["assets/home/images/web9/2.png","assets/home/images/web9/3.png","assets/home/images/web9/4.png"] },
  ];
  isPopup = false;
  show = "wrap-modal1 js-modal1 p-t-60 p-b-20 show-modal1";
  hide = "overlay-modal1 js-hide-modal1";
  isID :number;
  slideConfig = {"slidesToShow": 1, "slidesToScroll": 1};

  constructor() {}

  ngOnInit() {
  }

  toggleFilter(value: any) {
    console.log(value);
  }
  toggleSearch(value: any) {
    console.log(value);
  }
  openModal(id) {
    this.isID=id;
    this.isPopup = true;
  }

 closeModal() {
    this.isPopup = false;
  }

  fullScreen() {
    let elem = document.body;
    let image = document.getElementById('img')
    let methodToBeInvoked = elem.requestFullscreen || elem['mozRequestFullscreen']
      ||
      elem['msRequestFullscreen'];
    if (methodToBeInvoked) methodToBeInvoked.call(elem);
  }
  

}
