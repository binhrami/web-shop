import { Component } from "@angular/core";

@Component({
  selector: "seed-admin-root",
  templateUrl: "./app.component.html",
})
export class AppComponent {
  title = "seed-admin";
}
