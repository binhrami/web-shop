import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AuthGuard, SelectivePreloadingStrategy } from "@seed/common/core";

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        { path: "", redirectTo: "login", pathMatch: "full" },
        {
          path: "login",
          loadChildren: "@seed/ui-admin/login#UiAdminLoginModule"
        },
        {
          path: "",
          loadChildren: "@seed/ui-admin/layout-admin#LayoutAdminModule",
          canActivate: [AuthGuard]
        },
      ],
      {
        initialNavigation: "enabled",
        useHash: true,
        preloadingStrategy: SelectivePreloadingStrategy
      }
    )
  ],
  exports: [RouterModule],
  providers: [AuthGuard]

})
export class AppRoutingModule {}
