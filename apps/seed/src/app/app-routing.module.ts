import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SelectivePreloadingStrategy } from "@seed/common/core";

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        { path: "", redirectTo: "/home", pathMatch: "full" },
        {
          path: "error",
          loadChildren: "./error-page/error-page.module#ErrorPageModule"
        },
        {
          path: "home",
          loadChildren: "@seed/ui-seed/home#HomeModule"
        },
        {
          path: "**",
          redirectTo: "error/404"
        }
      ],
      {
        initialNavigation: "enabled",
        useHash: true,
        preloadingStrategy: SelectivePreloadingStrategy
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
